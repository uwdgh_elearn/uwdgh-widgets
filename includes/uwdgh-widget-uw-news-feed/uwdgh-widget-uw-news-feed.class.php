<?php

if ( !class_exists( 'UWDGH_Widget_UWNews' ) ) {

  class UWDGH_Widget_UWNews extends WP_Widget {

    static $is_boundless_theme;
    static $is_uw_wp_theme;

    /**
    * class constructor
    */
    function __construct() {
      parent::__construct(

      // Base ID of your widget
      'uwdgh_widget_uwnews',

      // Widget name will appear in UI
      __('UWDGH | UW News', 'uwdgh_widget_uwnews_domain'),

      // Widget description
      array( 'description' => __( 'Displays the UW News feed.', 'uwdgh_widget_uwnews_domain' ), )
      );


      // add widget stylesheet
      wp_enqueue_style( 'uwdgh_widget_uwnews', plugin_dir_url(__FILE__) . 'css/uwdgh-widget-uw-news-feed.css' );

      // check for boumndless theme
      self::$is_boundless_theme = self::is_boundless_theme();
      self::$is_uw_wp_theme = self::is_uw_wp_theme();
    }

    /**
     * Helper function
     * check if site uses UW boundless theme
     * @return boolean
     */
    function is_boundless_theme() {
      $this_theme = wp_get_theme();
      if ($this_theme->template == 'uw-2014') { return true; }
      return false;
    }
		
    /**
     * Helper function
     * check if site uses UW Wordpress theme
     * @return boolean
     */
    function is_uw_wp_theme() {
      $this_theme = wp_get_theme();
      if ($this_theme->template == 'uw_wp_theme') { return true; }
      return false;
    }

    // Creating widget front-end
    public function widget( $args, $instance ) {
      $title = apply_filters( 'widget_title', $instance['title'] );

      // before and after widget arguments are defined by themes
      echo $args['before_widget'];
      if ( ! empty( $title ) )
        echo $args['before_title'] . $title . $args['after_title'];

      // This is where you run the code and display the output
      echo __( self::widget_output( $instance ), 'uwdgh_widget_uwnews_domain' );

      echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {
			$title = 'New title';
			$feedid = 'uw';
			$feedurl = '';
			$items = 5;
			$show_date = false;
			$show_author = false;
			$show_summary = false;
			$show_image = false;
			$show_more = false;
			if (!empty( $instance )) {
				if ( isset( $instance[ 'title' ] ) ) {
					$title = $instance[ 'title' ];
				}
				else {
					$title = __( 'New title', 'uwdgh_widget_uwnews_domain' );
				}
				$feedid = $instance[ 'feedid' ];
				$feedurl    = esc_url( $instance[ 'feedurl' ] );
				($instance[ 'items' ]) ? $items = $instance[ 'items' ] :  $items = 5;
				$show_date = $instance[ 'show_date' ];
				$show_author = $instance[ 'show_author' ];
				$show_summary = $instance[ 'show_summary' ];
				$show_image = $instance[ 'show_image' ];
				$show_more = $instance[ 'show_more' ];
			}
      // Widget admin form
      ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>
      <p>
      <label for="<?php echo $this->get_field_id( 'feedid' ); ?>"><?php _e( 'Select a news feed:' ); ?>
        <select class='widefat' id="<?php echo $this->get_field_id('feedid'); ?>"
                name="<?php echo $this->get_field_name('feedid'); ?>" type="text">
          <option value='uw'<?php echo ($feedid=='uw')?'selected':''; ?>>
            University of Washington
          </option>
          <option disabled>— Dept. of Global Health feeds —</option>
          <option value='uw_gh'<?php echo ($feedid=='uw_gh')?'selected':''; ?>>
            Department of Global Health
          </option>
          <option value='uw_gh_sob'<?php echo ($feedid=='uw_gh_sob')?'selected':''; ?>>
            Department of Global Health Student Opportunities Blog
          </option>
          <option value='uw_gh_itech'<?php echo ($feedid=='uw_gh_itech')?'selected':''; ?>>
            International Training and Education Center for Health
          </option>
          <option value='uw_gh_cfar'<?php echo ($feedid=='uw_gh_cfar')?'selected':''; ?>>
						UW/Fred Hutch Center for AIDS Research
          </option>
          <option value='uw_gh_globalwach'<?php echo ($feedid=='uw_gh_globalwach')?'selected':''; ?>>
            Global Center for Integrated Health of Women, Adolescents, and Children
          </option>
          <option value='uw_gh_start'<?php echo ($feedid=='uw_gh_start')?'selected':''; ?>>
            UW START Center
          </option>
          <option value='uw_gh_globalhealthjustice'<?php echo ($feedid=='uw_gh_globalhealthjustice')?'selected':''; ?>>
						Global Health Justice Initiative
          </option>
          <option value='uw_gh_uwgmh'<?php echo ($feedid=='uw_gh_uwgmh')?'selected':''; ?>>
						Global Mental Health Program
          </option>
          <option value='uw_gh_globalcardio'<?php echo ($feedid=='uw_gh_globalcardio')?'selected':''; ?>>
						Global Cardiovascular Health Program
          </option>
          <option disabled>— other UW feeds —</option>
          <option value='uw_libraries_blog'<?php echo ($feedid=='uw_libraries_blog')?'selected':''; ?>>
            UW Libraries Blog
          </option>
          <option value='uw_foster_blog'<?php echo ($feedid=='uw_foster_blog')?'selected':''; ?>>
            Foster School of Business News Blog
          </option>
          <option value='uw_cs'<?php echo ($feedid=='uw_cs')?'selected':''; ?>>
            Allen School of Computer Science & Engineering
          </option>
          <option value='uw_jsis'<?php echo ($feedid=='uw_jsis')?'selected':''; ?>>
            Jackson School of International Studies
          </option>
          <option value='uw_nursing'<?php echo ($feedid=='uw_nursing')?'selected':''; ?>>
            School of Nursing
          </option>
          <option value='uw_sop'<?php echo ($feedid=='uw_sop')?'selected':''; ?>>
            School of Pharmacy
          </option>
          <option value='uw_evans'<?php echo ($feedid=='uw_evans')?'selected':''; ?>>
            Evans School of Public Policy & Governance
          </option>
        </select>
      </label><br>
      <b><i>or</i></b><br>
      <label for="<?php echo $this->get_field_id( 'feedurl' ); ?>"><?php _e( 'enter a RSS feed URL here:' ); ?>
  		    <input class="widefat" id="<?php echo $this->get_field_id( 'feedurl' ); ?>" name="<?php echo $this->get_field_name( 'feedurl' ); ?>" type="text" value="<?php echo $feedurl ?>" />
      </label>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'items' ) ?>"><?php _e('Number of items to display:'); ?>
          <select id="<?php echo $this->get_field_id( 'items' ) ?>" name="<?php echo $this->get_field_name( 'items' ); ?>" value="<?php echo $items ?>">
            <?php
                for ( $i = 1; $i <= 15; ++$i )
                  echo "<option value='$i' " . selected( $items, $i, false ) . ">$i</option>";
            ?>
          </select>
        </label>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display item date?' ); ?>
          <input type="checkbox" id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" <?php checked(  $show_date , true, true )  ?> />
        </label>
      </p>
      <?php if ( !self::$is_boundless_theme && !self::$is_uw_wp_theme ) : ?>
        <p>
          <label for="<?php echo $this->get_field_id( 'show_author' ); ?>"><?php _e( 'Display author?' ); ?>
            <input type="checkbox" id="<?php echo $this->get_field_id( 'show_author' ); ?>" name="<?php echo $this->get_field_name( 'show_author' ); ?>" <?php checked(  $show_author , true, true )  ?> />
          </label>
        </p>
        <p>
          <label for="<?php echo $this->get_field_id( 'show_summary' ); ?>"><?php _e( 'Display summary?' ); ?>
            <input type="checkbox" id="<?php echo $this->get_field_id( 'show_summary' ); ?>" name="<?php echo $this->get_field_name( 'show_summary' ); ?>" <?php checked(  $show_summary , true, true )  ?> />
          </label>
        </p>
      <?php endif ;?>
      <?php if ( self::$is_boundless_theme || self::$is_uw_wp_theme ) : ?>
        <p>
          <label for="<?php echo $this->get_field_id( 'show_image' ); ?>"><?php _e( 'Display item image (if available)?' ); ?>
            <input type="checkbox" id="<?php echo $this->get_field_id( 'show_image' ); ?>" name="<?php echo $this->get_field_name( 'show_image' ); ?>" <?php checked(  $show_image , true, true )  ?> />
          </label>
        </p>
        <p>
          <label for="<?php echo $this->get_field_id( 'show_more' ); ?>"><?php _e( 'Display "More" link?' ); ?>
            <input type="checkbox" id="<?php echo $this->get_field_id( 'show_more' ); ?>" name="<?php echo $this->get_field_name( 'show_more' ); ?>" <?php checked(  $show_more , true, true )  ?> />
          </label>
        </p>
      <?php endif ;?>
      <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['feedid'] = ( ! empty( $new_instance['feedid'] ) ) ? strip_tags( $new_instance['feedid'] ) : '';
		  $instance['feedurl'] = esc_url_raw( strip_tags( $new_instance['feedurl'] ) );
		  $instance['items'] = (int) ( $new_instance['items'] );
		  $instance['show_date'] = (bool) ( $new_instance['show_date'] );
		  $instance['show_author'] = (bool) ( $new_instance['show_author'] );
		  $instance['show_summary'] = (bool) ( $new_instance['show_summary'] );
		  $instance['show_image'] = (bool) ( $new_instance['show_image'] );
		  $instance['show_more'] = (bool) ( $new_instance['show_more'] );
      return $instance;
    }

    // widget output
    function widget_output( $instance ) {
      // echo '<pre>';
      // var_dump($instance);
      // echo '</pre>';
      $title = $instance['title'];
      $feedid = $instance['feedid'];
      $feedurl = $instance['feedurl'];
      $items = $instance['items'];
      $show_date = $instance['show_date'];
      $show_author = $instance['show_author'];
      $show_summary = $instance['show_summary'];
      $show_image = $instance['show_image'];
      $show_more = $instance['show_more'];

      if ($feedurl) {
        $url = $feedurl;
      } else {
        switch ($feedid) {
          case 'uw':
            $url = 'https://www.washington.edu/news/feed/';
            break;
          case 'uw_gh':
            $url = 'https://globalhealth.washington.edu/news/feed';
            break;
					case 'uw_gh_sob':
						$url = 'https://uwdgh.wordpress.com/feed/';
						break;
          case 'uw_gh_itech':
            $url = 'https://www.go2itech.org/feed/';
            break;
					case 'uw_gh_cfar':
						$url = 'https://depts.washington.edu/cfar/feed/';
						break;
          case 'uw_gh_globalwach':
            $url = 'https://depts.washington.edu/globalwach/news/feed/';
            break;
          case 'uw_gh_start':
            $url = 'http://uwstartcenter.org/news-recognition/feed/';
            break;
					case 'uw_gh_globalhealthjustice':
						$url = 'https://depts.washington.edu/globalhealthjustice/feed/';
						break;
					case 'uw_gh_uwgmh':
						$url = 'https://depts.washington.edu/uwgmh/feed/';
						break;
					case 'uw_gh_globalcardio':
						$url = 'https://depts.washington.edu/globalcardio/feed/';
						break;
          case 'uw_libraries_blog':
            $url = 'https://sites.uw.edu/libstrat/feed/';
            break;
          case 'uw_foster_blog':
            $url = 'https://blog.foster.uw.edu/category/news/feed/';
            break;
          case 'uw_cs':
            $url = 'https://news.cs.washington.edu/feed/';
            break;
          case 'uw_jsis':
            $url = 'https://jsis.washington.edu/news/feed/';
            break;
          case 'uw_nursing':
            $url = 'https://nursing.uw.edu/article/feed/';
            break;
          case 'uw_sop':
            $url = 'https://sop.washington.edu/feed/';
            break;
          case 'uw_evans':
            $url = 'https://evans.uw.edu/feed/';
            break;
          default:
            $url = 'https://www.washington.edu/news/feed/';
            break;
        }
      }
      if ( self::$is_boundless_theme || self::$is_uw_wp_theme ) {
        $booldate = ($show_date)?"true":"false";
        $boolimage = ($show_image)?"true":"false";
        $boolmore = ($show_more)?"true":"false";
        $shortcode = "[rss";
        $shortcode .= " url=" . $url;
        $shortcode .= " number=" . $items;
        $shortcode .= " show_date=" . $booldate;
        $shortcode .= " show_image=" . $boolimage;
        $shortcode .= " show_more=" . $boolmore;
        $shortcode .= "]";
        echo do_shortcode( $shortcode );
      } else {
        echo '<div class="uwdgh-widget-uwnews">';
        wp_widget_rss_output(array(
          'url' => $url,
          'title' => __( $title ),
          'items' => $items,
          'show_summary' => (bool)$show_summary,
          'show_author' => (bool)$show_author,
          'show_date' => (bool)$show_date,
        ));
        echo "</div>";
      }
    }

  }

}
