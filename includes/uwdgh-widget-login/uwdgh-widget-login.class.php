<?php

if ( !class_exists( 'UWDGH_Widget_Login' ) ) {

  class UWDGH_Widget_Login extends WP_Widget {

    /**
    * class constructor
    */
    function __construct() {
      parent::__construct(

      // Base ID of your widget
      'uwdgh_widget_login',

      // Widget name will appear in UI
      __('UWDGH | Login', 'uwdgh_widget_login_domain'),

      // Widget description
      array( 'description' => __( 'Displays a login or logout link.', 'uwdgh_widget_login_domain' ), )
      );
    }

    // Creating widget front-end
    public function widget( $args, $instance ) {
      $title = apply_filters( 'widget_title', $instance['title'] );

      // before and after widget arguments are defined by themes
      echo $args['before_widget'];
      if ( ! empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];

      // This is where you run the code and display the output
      echo __( self::widget_output(), 'uwdgh_widget_login_domain' );
      echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {
      if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
      }
      else {
        $title = __( 'New title', 'uwdgh_widget_login_domain' );
      }
      // Widget admin form
      ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>
      <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      return $instance;
    }

    // widget output
    function widget_output() {
      $this_theme = wp_get_theme();
      $is_boundless_theme = false;
      if ($this_theme->template == 'uw-2014') { $is_boundless_theme = true; }
      $is_uw_wp_theme = false;
      if ($this_theme->template == 'uw_wp_theme') { $is_uw_wp_theme = true; }
      ?>
      <?php if (is_user_logged_in()) : ?>
        <?php
        $current_user = wp_get_current_user();
        ?>
        <div>Signed-in as <em><?php echo  esc_html($current_user->display_name) ; ?></em></div>
        <?php if ($is_boundless_theme) : ?>
          <?php echo do_shortcode( '[button small="true" url="' . wp_logout_url(get_permalink()) . '"]Log out ' . $current_user->user_login . '[/button]' ); ?>
        <?php elseif ($is_uw_wp_theme) : ?>
          <?php echo do_shortcode( '[uw_button style="arrow" size="small" color="white" url="' . wp_logout_url(get_permalink()) . '"]Log out ' . $current_user->user_login . '[/uw_button]' ); ?>
        <?php else : ?>
          <a href="<?php echo wp_logout_url(get_permalink()); ?>">Log out</a>
        <?php endif;?>
      <?php else : ?>
        <?php if ($is_boundless_theme) : ?>
          <?php echo do_shortcode( '[button small="true" url="' . wp_login_url(get_permalink()) . '"]Log in[/button]' ); ?>
        <?php elseif ($is_uw_wp_theme) : ?>
          <?php echo do_shortcode( '[uw_button style="arrow" size="small" color="white" url="' . wp_login_url(get_permalink()) . '"]Log in[/uw_button]' ); ?>
        <?php else : ?>
          <a href="<?php echo wp_login_url(get_permalink()); ?>">Log in</a>
        <?php endif;?>
      <?php endif;?>
      <?php
    }

  }

}
