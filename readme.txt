=== UW DGH | Widgets ===
Contributors: dghweb, jbleys
Requires at least: 5.8
Tested up to: 6.4
Requires PHP: 7.4.24
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A widget library for your WordPress site.

== Description ==
A widget library for your WordPress site.

**Widgets**
- UWDGH | Login Widget<br>
provides a Login/Logout link for the user.

- UWDGH | UW News Widget<br>
retrieves news items from UW news feeds.


== Changelog ==
**[unreleased]**

#### 0.2.6 / 2024-03-10
* Add CFAR feed

#### 0.2.5 / 2024-03-10
* Tested for WP 6.4
* Plugin header update

#### 0.2.4 / 2023-06-27
* Tested for PHP 8.x
* Tested for WP 6.2
* Bug fixes

#### 0.2.3 / 2022-12-20
* Updated widgets for UW's "uw_wp_theme" theme
* Added feed options to the "UWDGH | UW News" widget

#### 0.2.2 / 2022-03-23
* Plugin header update

#### 0.2.1 / 2022-02-23
* Bug fixes

#### 0.2 / 2021-10-15
* Added CSS layout fixes to UW News Widget
* Added feed options to UW News Widget

#### 0.1.6 / 2021-06-29
* Added feed options to UW News Widget

#### 0.1.5 / 2021-06-25
* Added UW News Widget
* consolidated changelog

#### 0.1.4 / 2020-08-13
* Added plugin icon

#### 0.1.3 / 2020-08-13
* Added changelog and plugin banner

#### 0.1.2 / 2019‑09‑18
* updated the Login Widget with UW Boundless theme button styles

#### 0.1.1 / 2019‑06‑11
* added username to Login Widget

#### 0.1 / 2019‑06‑11
* Initial commit, including Login Widget
