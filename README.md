# README #

This README document contains steps necessary to use the _UW DGH | Widgets_ WordPress plugin

## What is this repository for? ##

* Adds a widget library to your WordPress site.

## How do I get set up? ##

* Navigate to your wp-content/plugins/ folder
* `git clone https://bitbucket.org/uwdgh_elearn/uwdgh-widgets`
Alternatively you can use the [Git Updater](https://github.com/afragen/git-updater) plugin to manage themes and plugins not hosted on WordPress.org.

## Who do I talk to? ##

* dghweb@uw.edu