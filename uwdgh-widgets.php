<?php
/*
Plugin Name:  UW DGH | Widgets
Plugin URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-widgets
Update URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-widgets
Description:  A widget library for your Wordpress site.
Author:       Department of Global Health - University of Washington
Author URI:   https://depts.washington.edu/dghweb/
Version:      0.2.6
License:      GPLv2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  uwdgh-widgets
Domain Path:
Bitbucket Plugin URI:  https://bitbucket.org/uwdgh_elearn/uwdgh-widgets

"UW DGH | Widgets" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

"UW DGH | Widgets" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "UW DGH | Widgets". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( ! defined( 'UWDGH_Widgets_PLUGIN' ) ) {
	define( 'UWDGH_Widgets_PLUGIN', plugin_basename( __FILE__ ) ); // Plugin
}

if ( !class_exists( 'UWDGH_Widgets' ) ) {

  // includes
  include( plugin_dir_path(__FILE__) . 'includes/uwdgh-widget-login/uwdgh-widget-login.class.php');
  include( plugin_dir_path(__FILE__) . 'includes/uwdgh-widget-uw-news-feed/uwdgh-widget-uw-news-feed.class.php');

  class UWDGH_Widgets {

    /**
    * class initializaton
    */
    function __construct() {
      // Add plugin settings link to Plugins page
      add_filter( 'plugin_action_links_'.UWDGH_Widgets_PLUGIN, array( __CLASS__, 'uwdgh_widgets_add_settings_link' ) );

      // register each widget as a setting that can be enabled/disabled
      register_setting('uwdgh_widgets_options','uwdgh_widgets_options_widget_login', array('default' => 0,));
      register_setting('uwdgh_widgets_options','uwdgh_widgets_options_widget_uwnews', array('default' => 0,));

      // load widgets
      add_action( 'widgets_init', array( __CLASS__, 'uwdgh_widgets_registration' ) );

      // add menu item
      add_action( 'admin_menu' , array( __CLASS__, 'uwdgh_widgets_settings' ) );

      // register uninstall hook
      if( function_exists('register_uninstall_hook') )
        register_uninstall_hook(__FILE__,array( __CLASS__, 'uwdgh_widgets_uninstall' ));
    }

    /**
    * Add settings link
    */
    static function uwdgh_widgets_add_settings_link( $links ) {
      $settings_link = '<a href="options-general.php?page=uwdgh-widgets">' . __( 'Settings' ) . '</a>';
      array_push( $links, $settings_link );
      return $links;
    }

    /**
    * widget registration
    */
    static function uwdgh_widgets_registration() {
      // register each separate widget class
      if ( get_option('uwdgh_widgets_options_widget_login') )
        register_widget( 'UWDGH_Widget_Login' );
      if ( get_option('uwdgh_widgets_options_widget_uwnews') )
        register_widget( 'UWDGH_Widget_UWNews' );
    }

    /**
    * Admin menu item
    */
    static function uwdgh_widgets_settings() {
      add_options_page('UW DGH | Widgets','UW DGH | Widgets','manage_options','uwdgh-widgets',array( __CLASS__, 'uwdgh_widgets_options_page' ));
    }

    /**
    * Options form
    */
    static function uwdgh_widgets_options_page() {
      ?>
      <div class="wrap">
        <h2><?php _e('UW DGH | Widgets','uwdgh-widgets');?></h2>
        <p><?php _e('Here you enable or disable available widgets.','uwdgh-widgets');?></p>
        <form action="options.php" method="post" id="uwdgh-widgets-options-form">
          <?php settings_fields('uwdgh_widgets_options'); ?>
          <table class="form-table">
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_widgets_options_widget_login">
                  <?php _e('UWDGH | Login Widget','uwdgh-widgets');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_widgets_options_widget_login" name="uwdgh_widgets_options_widget_login"  value="1" <?php checked(1, get_option('uwdgh_widgets_options_widget_login'), true); ?> />
                <span><em>(Default: unchecked)</em></span><br>
                <em>This widget provides a Login/Logout link for the user.</em>
              </td>
            </tr>
            <tr class="odd" valign="top">
              <th scope="row">
                <label for="uwdgh_widgets_options_widget_uwnews">
                  <?php _e('UWDGH | UW News Widget','uwdgh-widgets');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_widgets_options_widget_uwnews" name="uwdgh_widgets_options_widget_uwnews"  value="1" <?php checked(1, get_option('uwdgh_widgets_options_widget_uwnews'), true); ?> />
                <span><em>(Default: unchecked)</em></span><br>
                <em>This widget retrieves the latest news items from various UW feeds. Default is the main UW news feed.</em>
              </td>
            </tr>
          </table>
          <?php submit_button(); ?>
        </form>
      <?php
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_widgets_uninstall() {
      // delete any options
      delete_option('uwdgh_widgets_options_widget_login');
      delete_option('uwdgh_widgets_options_widget_uwnews');
    }

  }

  New UWDGH_Widgets();

}
